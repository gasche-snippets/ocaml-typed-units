(** A small draft of unit-aware multiplication with phantom types, but
    using explicit term witnesses to express type-level addition. *)

(* type-level integers, and explicitly-witnessed sums *)
type z
type 'a s

module Sum : sig
  (* a + b = c *)
  type ('a, 'b, 'c) sum 

  (* a + 0 = a *)
  val s0 : ('a, z, 'a) sum

  (* a + (b+1) = (a+b)+1 *)
  val s1 : ('a, 'b, 'c) sum -> ('a, 'b s, 'c s) sum
end = struct
  type ('a, 'b, 'c) sum = unit
  let s0 = ()
  let s1 () = ()
end

include Sum

(* Units; in this simple case, there is only one unit named 'u' *)
module Units : sig
  type 'a u
  type 'a num

  val u : float -> (z s) num
  val f : 'a num -> float

  val add : 'a num -> 'a num -> 'a num
  val mult : ('a, 'b, 'c) sum -> 'a num -> 'b num -> 'c num
end = struct
  type 'a u = unit
  type 'a num = float

  let u x = x
  let f x = x

  let add x y = x +. y
  let mult _ x y = x *. y
end

include Units

let u1 = u 2. (* z s num *)
let u2 = mult (s1 s0) u1 u1 (* z s s num *)

let u1pu1 = add u1 u1
let u1pu2 = (* type error *) add u1 u2
