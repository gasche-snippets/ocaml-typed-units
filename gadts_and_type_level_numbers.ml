(** type-level unary natural numbers *)
type z
type 'a s

(** ... injected from the term level by a GADT *)
type _ nat =
  | Z : z nat
  | S : 'n nat -> 'n s nat

(** sized-lists (ornamented natural numbers?) *)
type ('a, _) sized_list =
  | Nil : ('a, z) sized_list
  | Cons : 'a * ('a, 'n) sized_list -> ('a, 'n s) sized_list

let rec make_list : type a n . a -> n nat -> (a, n) sized_list =
  fun x -> function
    | Z -> Nil
    | S n -> Cons(x, make_list x n)

let rec size : type a n . (a, n) sized_list -> n nat = function
  | Nil -> Z
  | Cons (_, li) -> S (size li)

let rec map2 : type a b c n .
  (a -> b -> c) -> (a, n) sized_list -> (b, n) sized_list -> (c, n) sized_list
= fun f la lb -> match la, lb with
  | Nil, Nil -> Nil
  | Cons (a, la), Cons (b, lb) -> Cons (f a b, map2 f la lb)


(** Term-witnessed sum of two type-level natural numbers:
   (a, b, c) sum:   a + b = c *)
type (_, _, _) sum =
  (** a + 0 = a *)
  | Add0 : ('a, z, 'a) sum
  (** a + b = c  ⇒  a + (b+1) = c+1 *)
  | AddS : ('a, 'b, 'c) sum -> ('a, 'b s, 'c s) sum

let rec adder : type a b c . (a, b, c) sum -> a nat -> b nat -> c nat = function
  | Add0 -> (fun a Z -> a)
  | AddS s -> (fun a (S b) -> S (adder s a b))

(** But:
    - heavy
    - how to get commutativity? <- Inject it into the type
      | AddComm : (a, b, c) sum -> (b, a, c) sum *)


(** A different approach: numbers with addition precomputed in the type,
    to avoid the need for term-level witnesses
      (a, b) add_nat : a+n = b
*)
type (_, _) add_nat =
  | AZ : ('a, 'a) add_nat
  | AS : ('a, 'b) add_nat -> ('a, 'b s) add_nat

(** ('a, 'a s s) add_nat;
   as we get a polymorphic term, we can add it to any other term,
   the 'a variable will be unified with the 
*)
let test = AS (AS AZ)

let rec add : type a b c . (a, b) add_nat -> (b, c) add_nat -> (a, c) add_nat =
  fun a -> function
    | AZ -> a
    | AS b -> AS (add a b)

let rec add_to_nat : type a b . (a, b) add_nat -> a nat -> b nat = function
  | AZ -> (fun a -> a)
  | AS n -> (fun a -> S (add_to_nat n a))

(** (z, 'a) add_nat -> 'a nat *)
let to_nat x = add_to_nat x Z

